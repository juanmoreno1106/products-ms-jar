package com.arqteamip.productsms.entities;

import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String account_type;
    private String account_sub_type;
    private String account_id;
    private String account_number;
    private String parent_account;
    private String countable_balance;
    private String available_balance;
    private String overdraft_balance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getAccount_sub_type() {
        return account_sub_type;
    }

    public void setAccount_sub_type(String account_sub_type) {
        this.account_sub_type = account_sub_type;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getParent_account() {
        return parent_account;
    }

    public void setParent_account(String parent_account) {
        this.parent_account = parent_account;
    }

    public String getCountable_balance() {
        return countable_balance;
    }

    public void setCountable_balance(String countable_balance) {
        this.countable_balance = countable_balance;
    }

    public String getAvailable_balance() {
        return available_balance;
    }

    public void setAvailable_balance(String available_balance) {
        this.available_balance = available_balance;
    }

    public String getOverdraft_balance() {
        return overdraft_balance;
    }

    public void setOverdraft_balance(String overdraft_balance) {
        this.overdraft_balance = overdraft_balance;
    }
}
