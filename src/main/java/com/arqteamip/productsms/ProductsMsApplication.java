package com.arqteamip.productsms;

import com.arqteamip.productsms.entities.Product;
import com.arqteamip.productsms.repository.ProductsRepository;
import com.arqteamip.productsms.rest.models.response.RestResponse;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.converter.json.GsonFactoryBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

@SpringBootApplication
@RestController
public class ProductsMsApplication {
	public static void main(String[] args) {
		SpringApplication.run(ProductsMsApplication.class, args);
	}
}
