package com.arqteamip.productsms.rest.models.response;

import com.arqteamip.productsms.entities.Product;
import net.bytebuddy.implementation.bind.annotation.Default;

import java.io.Serializable;

public class RestResponse implements Serializable {
    Integer status;
    String msg="";
    Iterable<Product> payload;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Iterable<Product> getPayload() {
        return payload;
    }

    public void setPayload(Iterable<Product> payload) {
        this.payload = payload;
    }
}
