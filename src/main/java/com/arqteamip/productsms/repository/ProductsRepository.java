package com.arqteamip.productsms.repository;

import com.arqteamip.productsms.entities.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductsRepository extends CrudRepository<Product, Integer> {
}
